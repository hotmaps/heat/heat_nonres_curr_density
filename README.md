[![DOI](https://zenodo.org/badge/doi/10.5281/zenodo.4687113.svg)](https://doi.org/10.5281/zenodo.4687113)

# Heat density map (final energy demand for heating and DHW) of non-residential buildings in EU28 + Switzerland, Norway and Iceland for the year 2015


## Repository structure
```
data                    -- containts the dataset in Geotiff format
readme.md               -- Readme file 
datapackage.json        -- Includes the meta information of the dataset for processing and data integration

```


## Documentation
This dataset shows the final energy demand for heating and hot water of non-residential buildings in EU28 on hectare (ha) level. 

A core element of the Hotmaps project is the development of a heat density map on hectare level  for  the  EU28  countries.  We  achieved  this  goal  by  developing  a  new  approach,  which  correlates  information  on  the  local  built  environment  with  its  FED  for  SH,  SC  and  DHW generation. To do so, we derived a spatial distribution function based on similar indicators as used for the NUTS0 to NUTS3 transformation. Again, the approach builds on the central idea that the UED correlates with the population number within a certain plot area, the economic activity and the climatic conditions.

An overview on the process and main data sources used is illustrated in this [Figure](./schematic.png).

The heat density map will be used in the Hotmaps toolbox as a fundamental dataset for various calculation modules such as calculation of district heating potential or district heat grid cost estimation.

For detailed explanations and a graphical illustration of the dataset please see the [Hotmaps WP2 report](https://www.hotmaps-project.eu/wp-content/uploads/2018/03/D2.3-Hotmaps_for-upload_revised-final_.pdf) section 2.1.3 page 33 and [this paper](https://doi.org/10.3390/en12244789).


### Limitations of the dataset
For the use and the estimation of the reliability of the data it is important to keep in mind that the data maps build on a statistical approach and do not take site specific or local conditions into account.
Regarding the final energy demand map (heat  density  map), the  data  are calculated  on the  NUTS0 level from statistical data on the energy consumption as well as the national building stock characteristics. In order to derive grid cell specific energy demand-per-floor area data, we assessed the surface-to-volume ratio of buildings based on the OpenStreetMap database, the share per construction periods as well as the heating and cooling degree days. The first two indicators are plausible, but in the end highly uncertain. Therefore, we put only a low weight on these indicators in our calculations. The last indicator, the heating and cooling degree days are of higher accuracy, even though we used a  simple atmospheric temperature lapse rate  model,  which  cannot  account  for  local  site  specific  weather  and  thus  climate  conditions. However, additional uncertainty stem from the fact, that is unknown to which degree colder (or warmer) local climate conditions already factored in the construction of building. Since we assume  that  this  might  be  the  case  to  some  extent,  we  lowered  the  weight  of  the  climate  indicator compared to what is usually considered to be actual degree of influence. Again, data quality checks indicate that results are plausible, however we recommend to use individual data on the heated area-specific final energy demand whenever local data are available. 
Furthermore, the ratio between energy needs (useful energy demand) and final energy consumption is being keept constant with a country. We therefore do not consider the local energy mix and the conversion efficiency that associate with different types of heating systems / energy carriers. 
The data on NUTS0 level are based on heating degree days correct energy consumption data.

### References
Müller, A., Hummel, M., Kranzl, L., Fallahnejad, M., Büchele, R., 2019. Open Source Data for Gross Floor Area and Heat Demand Density on the Hectare Level for EU 28. Energies 12, 4789. https://doi.org/10.3390/en12244789


## How to cite
Müller, A., Hummel, M., Kranzl, L., Fallahnejad, M., Büchele, R., 2019. Open Source Data for Gross Floor Area and Heat Demand Density on the Hectare Level for EU 28. Energies 12, 4789. https://doi.org/10.3390/en12244789


## Authors
Andreas Mueller, Mostafa Fallahnejad <sup>*</sup>

<sup>*</sup> [TU Wien, EEG](https://eeg.tuwien.ac.at/), Institute of Energy Systems and Electrical Drives, Gusshausstrasse 27-29/370, 1040 Wien


## License
## License
Copyright © 2016-2020: Andreas Mueller
 
Creative Commons Attribution 4.0 International License
This work is licensed under a Creative Commons CC BY 4.0 International License.

SPDX-License-Identifier: CC-BY-4.0

License-Text: https://spdx.org/licenses/CC-BY-4.0.html

## Acknowledgment
We would like to convey our deepest appreciation to the Horizon 2020 [Hotmaps Project](http://www.hotmaps-project.eu/) (Grant Agreement number 723677), which provided the funding to carry out the present investigation.

